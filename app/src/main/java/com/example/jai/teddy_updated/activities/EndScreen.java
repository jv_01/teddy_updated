package com.example.jai.teddy_updated.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.jai.teddy_updated.R;
import java.util.ArrayList;

public class EndScreen extends AppCompatActivity {

    private int score;
    private SharedPreferences sharedPref;
    private String question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_screen);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Handlee-Regular.ttf");

        sharedPref = getSharedPreferences(LandingPage.PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        score = getIntent().getIntExtra("score", 0);
        question = getIntent().getStringExtra("question");

        Button restart = (Button) findViewById(R.id.button16);
        TextView message = (TextView) findViewById(R.id.textView8);
        TextView wrong = (TextView) findViewById(R.id.textView7);

        restart.setTypeface(custom_font);
        message.setTypeface(custom_font);
        wrong.setTypeface(custom_font);

        if (score > LandingPage.high_score) {
            LandingPage.high_score = score;
            editor.putInt("high_score", score);
            editor.commit();
            Toast.makeText(getApplicationContext(), "New Highscore " + score, Toast.LENGTH_SHORT).show();
        }

        if(score!=0)
            LandingPage.db.addItem(score,"Jai_Test");

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(EndScreen.this, GameScreen.class));
            }
        });

        message.setText(Html.fromHtml(question.trim() + "<br>" + "=" + "<br><big>" + GameScreen.result + "</big>"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(LandingPage.play_status) {
            if (LandingPage.mediaPlayer.isPlaying()) {
                LandingPage.mediaPlayer.pause();
                LandingPage.playing_position = LandingPage.mediaPlayer.getCurrentPosition();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(EndScreen.this, LandingPage.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(LandingPage.play_status) {
            if (!LandingPage.mediaPlayer.isPlaying()) {
                LandingPage.mediaPlayer.start();
                LandingPage.mediaPlayer.seekTo(LandingPage.playing_position);
            }
        }
    }
}