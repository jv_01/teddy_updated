package com.example.jai.teddy_updated.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.models.Score;

import java.util.HashMap;
import java.util.Random;

public class GameScreen extends AppCompatActivity {

    private String output;
    public static int result = 0;
    private int count = 1, user_input = 0, score = 0;
    private int s1 =1, s2 = 2, s3 = 3, s4 = 4, s5 = 5, s6 = 6, s7 = 7, s8 = 8, s9 = 9, s0 = 10, sAns = 11, sMin = 12;
    private CountDownTimer cdt;
   // private MediaPlayer newMediaPlayer;
    private ImageView timerView;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_screen);

        soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);
        soundPoolMap = new HashMap<>();
        soundPoolMap.put(s1, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s2, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s3, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s4, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s5, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s6, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s7, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s8, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s9, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(s0, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(sAns, soundPool.load(this, R.raw.beep, 1));
        soundPoolMap.put(sMin, soundPool.load(this, R.raw.beep, 1));

        if(LandingPage.mediaPlayer.isPlaying()) {
           /* AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);*/
            LandingPage.mediaPlayer.setVolume(0.5f , 0.5f);
        }
        /*newMediaPlayer = MediaPlayer.create(this, R.raw.beep);*/
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Handlee-Regular.ttf");

        final EditText answer = (EditText) findViewById(R.id.editText);
        final TextView question = (TextView) findViewById(R.id.textView2);
        final TextView score_text = (TextView) findViewById(R.id.textView4);
        ImageView life1 = (ImageView) findViewById(R.id.imageView2);
        ImageView life2= (ImageView) findViewById(R.id.imageView2);
        TextView scoreTv = (TextView) findViewById(R.id.textView3);
        TextView scoreData = (TextView) findViewById(R.id.textView4);
        timerView = (ImageView) findViewById(R.id.imageView5);

        answer.setTypeface(custom_font);
        question.setTypeface(custom_font);
        scoreTv.setTypeface(custom_font);
        scoreData.setTypeface(custom_font);

        if(score >= 20) {
            life1.setImageResource(R.drawable.heart);
            life2.setImageResource(R.drawable.empty_heart);
        } else if (score >= 40) {
            life1.setImageResource(R.drawable.heart);
            life2.setImageResource(R.drawable.heart);
        }

        try {
            score = LandingPage.save_point.getScore();
            score_text.setText("" + score);
        } catch (NullPointerException npe) {

        }

        answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(answer.getWindowToken(), 0);
            }
        });

        answer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (answer.getRight() - answer.getCompoundDrawables()[DRAWABLE_RIGHT].
                            getBounds().width())) {
                        play_music(sAns);
                        int end = answer.getSelectionEnd();
                        if (end != 0) {
                            output = answer.getText().toString();
                            String selected_string = output.substring(end - 1, end);
                            output = output.replaceFirst(selected_string, "");
                            answer.setText(output);
                            answer.setSelection(answer.getText().length());
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        Button b1 = (Button) findViewById(R.id.button2);
        Button b2 = (Button) findViewById(R.id.button3);
        Button b3 = (Button) findViewById(R.id.button4);
        Button b4 = (Button) findViewById(R.id.button5);
        Button b5 = (Button) findViewById(R.id.button6);
        Button b6 = (Button) findViewById(R.id.button7);
        Button b7 = (Button) findViewById(R.id.button8);
        Button b8 = (Button) findViewById(R.id.button9);
        Button b9 = (Button) findViewById(R.id.button10);
        Button b0 = (Button) findViewById(R.id.button12);
        Button exc = (Button) findViewById(R.id.button13);
        Button bm = (Button) findViewById(R.id.button11);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s1);
                int value = 1;
                setValue(value, answer);

            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s2);
                int value = 2;
                setValue(value, answer);
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s3);
                int value = 3;
                setValue(value, answer);
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s4);
                int value = 4;
                setValue(value, answer);
            }
        });

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s5);
                int value = 5;
                setValue(value, answer);
            }
        });

        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s6);
                int value = 6;
                setValue(value, answer);
            }
        });

        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s7);
                int value = 7;
                setValue(value, answer);
            }
        });

        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s8);
                int value = 8;
                setValue(value, answer);
            }
        });

        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s9);
                int value = 9;
                setValue(value, answer);
            }
        });

        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(s0);
                int value = 0;
                setValue(value, answer);
            }
        });

        bm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_music(sMin);
                Editable input = answer.getText();
                if (input != null) {
                    String value = input.toString();
                    if (!value.contains("-")) {
                        input.insert(0, "-");
                        answer.setText(input);
                        answer.setSelection(answer.getText().length());
                    } else {
                        String selected_string = value.substring(0, 1);
                        value = value.replace(selected_string, "");
                        answer.setText(value);
                        answer.setSelection(answer.getText().length());
                    }
                } else {
                    answer.setText("-");
                    answer.setSelection(1);
                }
            }
        });

        set_question(question, exc, answer, score_text);
    }

    private void play_music(int id) {
        if(LandingPage.music_status) {
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            float curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            float leftVolume = curVolume / maxVolume;
            float rightVolume = curVolume / maxVolume;
            int priority = 1;
            int no_loop = 0;
            float normal_playback_rate = 1f;
            soundPool.play(soundPoolMap.get(id), leftVolume, rightVolume, priority, no_loop, normal_playback_rate);
        }
    }

    private void setValue(int value, EditText answer) {
        Editable data = answer.getText();
        int start = answer.getSelectionStart();
        data.insert(start, String.valueOf(value));
        answer.setText(data.toString());
        answer.setSelection(answer.getText().length());
    }


    private void set_question(final TextView question, final Button exc, final EditText answer, final TextView score_text) {
        /*final TextView timer_field = (TextView) findViewById(R.id.textView6);*/

        question.setText(getRandom());

        cdt = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                /*timer_field.setText("" + millisUntilFinished / 1000);*/
                drawTimerView(millisUntilFinished / 1000);
            }

            public void onFinish() {
                create_save();
                LandingPage.playing_position = LandingPage.mediaPlayer.getCurrentPosition();
                Intent intent = new Intent(GameScreen.this, EndScreen.class);
                intent.putExtra("score", score);
                intent.putExtra("question", question.getText().toString());
                startActivity(intent);
                finish();
            }
        }.start();

        exc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = answer.getText().toString();
                if (!value.equals("")) {
                    user_input = Integer.parseInt(value);
                    if (user_input == result) {
                        answer.setText("");
                        output = "";
                        score++;
                        score_text.setText("" + score);
                        cdt.cancel();
                        cdt = null;
                        set_question(question, exc, answer, score_text);
                    } else {
                        cdt.cancel();
                        cdt = null;
                        create_save();
                        LandingPage.playing_position = LandingPage.mediaPlayer.getCurrentPosition();
                        Intent intent = new Intent(GameScreen.this, EndScreen.class);
                        intent.putExtra("score", score);
                        intent.putExtra("question", question.getText().toString());
                        startActivity(intent);
                        finish();
                    }
                } else
                    Toast.makeText(getApplicationContext(), "Enter answer!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void drawTimerView(long l) {
       // int angle = (360 - (int)(18 * l));
        int angle = (int)(18 * l);
        ShapeDrawable timer = new ShapeDrawable(new ArcShape(0, angle));
        timer.setIntrinsicHeight(100);
        timer.setIntrinsicWidth(100);
        timer.getPaint().setColor(Color.WHITE);
        timerView.setImageDrawable(timer);
    }

    private void create_save() {
        if (score >= 20)
            LandingPage.save_point.setScore(20);
        else if (score >= 40)
            LandingPage.save_point.setScore(40);
    }

    private String getRandom() {
        Random r = new Random();
        result = 0;
        int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
        String question = "", operator = null;
        if (score < 20) {
            n1 = r.nextInt(100 - 1);
            n2 = r.nextInt(100 - 1);
            n3 = r.nextInt(10 - 1);
        } else if (score >= 20 && score <= 40) {
            n1 = r.nextInt(1000 - 1);
            n2 = r.nextInt(1000 - 1);
            n3 = r.nextInt(10 - 1);
        } else {
            n1 = r.nextInt(10000 - 1);
            n2 = r.nextInt(10000 - 1);
            n3 = r.nextInt(1000 - 1);
            n4 = r.nextInt(100 - 1);
        }

        if (score % 5 == 0 && score != 0) {
            if (count == 1) {
                operator = "-";
                result = n1 - n2;
                question = n1 + " " + operator + " " + n2;
                count++;
            } else if (count == 2) {
                if (score <= 40) {
                    operator = "*";
                    result = n1 * n3;
                    question = n1 + " " + operator + " " + n3;
                } else {
                    operator = "*";
                    result = n3 * n4;
                    question = n3 + " " + operator + " " + n4;
                }
                count = 1;
            }
        } else {
            operator = "+";
            result = n1 + n2;
            question = n1 + " " + operator + " " + n2;
        }
        return question;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        cdt.cancel();
        cdt = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(LandingPage.play_status) {
            if (LandingPage.mediaPlayer.isPlaying()) {
                LandingPage.mediaPlayer.pause();
                LandingPage.playing_position = LandingPage.mediaPlayer.getCurrentPosition();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(LandingPage.play_status) {
            if (!LandingPage.mediaPlayer.isPlaying()) {
                LandingPage.mediaPlayer.start();
                LandingPage.mediaPlayer.seekTo(LandingPage.playing_position);
            }
        }
    }
}

