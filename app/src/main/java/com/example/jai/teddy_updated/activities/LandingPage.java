package com.example.jai.teddy_updated.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.adapters.DbHelper;
import com.example.jai.teddy_updated.fragments.TabCreationFragment;
import com.example.jai.teddy_updated.models.Score;

import java.io.IOException;

public class LandingPage extends AppCompatActivity {

    public static final String PREFS_NAME = "Game_Assets";
    public static final int MAX_VOLUME = 100;
    public static Score save_point;
    public static int high_score;
    public static DbHelper db;
    public static int playing_position;
    public static MediaPlayer mediaPlayer;
    public static boolean play_status;
    public static boolean music_status;

    private boolean initialFlag = true;
    private AssetFileDescriptor assetFileDescriptor;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private AudioManager am;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        save_point = new Score();
        db = new DbHelper(this);

        sharedPref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = sharedPref.edit();
        high_score = sharedPref.getInt("high_score", 0);
        play_status = sharedPref.getBoolean("sound_status", true);
        music_status = sharedPref.getBoolean("music_status", true);

        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        int result = am.requestAudioFocus(mAudioFocusListener,
                AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {

            if (initialFlag) {
                try {
                    mediaPlayer = new MediaPlayer();
                    assetFileDescriptor = getAssets().openFd("bensound-cute.mp3");
                    mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(),
                            assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
                    mediaPlayer.setLooping(true);
                    mediaPlayer.prepare();
                    initialFlag = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                editor.putBoolean("play_status", play_status).apply();
            }
            start_music();
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new TabCreationFragment()).commit();
        }
    }

    public static void start_music() {
        if(mediaPlayer.isPlaying()){
            if(!play_status) {
                mediaPlayer.pause();
            }
        }
        else {
            if (play_status) {
                mediaPlayer.start();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(play_status) {
            mediaPlayer.stop();
            mediaPlayer = null;
            am.abandonAudioFocus(mAudioFocusListener);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(play_status) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                playing_position = mediaPlayer.getCurrentPosition();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(play_status) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                mediaPlayer.seekTo(playing_position);
            }
        }
    }

    private AudioManager.OnAudioFocusChangeListener mAudioFocusListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {

            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS:
                    mediaPlayer.stop();
                    mediaPlayer = null;
                    am.abandonAudioFocus(mAudioFocusListener);
                    break;

                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    mediaPlayer.pause();
                    break;

                case AudioManager.AUDIOFOCUS_GAIN:
                    mediaPlayer.start();
                    break;
            }

        }
    };

}