package com.example.jai.teddy_updated.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Statistics";
    final static String TABLE_NAME = "High_Score";
    final static String SCORE = "score";
    final static String NAME = "name";
    final static String ID = "_id";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String DATABASE_TABLE_CREATE = "create table if not exists " + TABLE_NAME + "( " + ID + " integer primary key autoincrement, " + SCORE + " integer not null , " + NAME + " text not null)";
        db.execSQL(DATABASE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
    }

    public void addItem(Integer score, String name) {

        int id, min = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        ContentValues values = new ContentValues();
        values.put("score", score);
        values.put("name", name);
        if (count < 10)
            db.insert(TABLE_NAME, null, values);
        else {
            Cursor cursor = db.query(TABLE_NAME, new String[]{ID, SCORE, NAME}, null, null, null, null, SCORE + " ASC");
            cursor.moveToFirst();
            if (!cursor.equals(null)) {
                min = Integer.parseInt(cursor.getString(1));
                if (score > min) {
                    id = Integer.parseInt(cursor.getString(0));
                    db.update(TABLE_NAME, values, ID + "=?", new String[]{id + ""});
                    //db.update(TABLE_NAME, values, ID + "=?" + id, null);
                }
            }
            cursor.close();
        }
    }

    public ArrayList<String> getItems() {
        ArrayList<String> scores_list = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{SCORE, NAME}, null, null, null, null, SCORE + " DESC");
        cursor.moveToFirst();
        if (cursor.moveToFirst()) {
            do {
                String data = cursor.getString(0) + "/" + cursor.getString(1);
                scores_list.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return scores_list;
    }
}

