package com.example.jai.teddy_updated.adapters;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.activities.LandingPage;

import java.util.ArrayList;
import java.util.ListResourceBundle;

/**
 * Created by jai on 10/11/15.
 */
public class ScoreListAdapter extends BaseAdapter {

    private ArrayList<String> high_score;
    private Activity activity;
    private Typeface custom_font;

    public ScoreListAdapter(Activity activity, ArrayList<String> high_score, Typeface custom_font) {
        this.high_score = high_score;
        this.activity = activity;
        this.custom_font = custom_font;
    }

    @Override
    public int getCount() {
        return high_score.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = view;
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            rowView = layoutInflater.inflate(R.layout.each_score_field, null);
            TextView score = (TextView) rowView.findViewById(R.id.score);
            TextView name = (TextView) rowView.findViewById(R.id.name);
            score.setTypeface(custom_font);
            name.setTypeface(custom_font);
            String each_score = high_score.get(i);
            String[] values = each_score.split("/");
            score.setText(values[0]);
            name.setText(values[1]);
        }
        return rowView;
    }
}
