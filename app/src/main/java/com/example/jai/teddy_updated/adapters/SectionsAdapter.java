package com.example.jai.teddy_updated.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.jai.teddy_updated.fragments.ScoreFragment;
import com.example.jai.teddy_updated.fragments.SettingsFragment;
import com.example.jai.teddy_updated.fragments.StartScreenFragment;

/**
 * Created by jai on 3/11/15.
 */
public class SectionsAdapter extends FragmentPagerAdapter {
    private Activity activity;

    public SectionsAdapter(FragmentActivity activity, FragmentManager childFragmentManager) {
        super(childFragmentManager);
        this.activity = activity;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ScoreFragment fragment1 = new ScoreFragment();
                return fragment1;
            case 1:
                StartScreenFragment fragment2 = new StartScreenFragment();
                return fragment2;
            case 2:
                SettingsFragment fragment3 = new SettingsFragment();
                return fragment3;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
