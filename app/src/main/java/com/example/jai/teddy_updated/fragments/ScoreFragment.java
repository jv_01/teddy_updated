package com.example.jai.teddy_updated.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.activities.GameScreen;
import com.example.jai.teddy_updated.activities.LandingPage;
import com.example.jai.teddy_updated.adapters.ScoreListAdapter;

import java.util.ArrayList;

/**
 * Created by jai on 3/11/15.
 */
public class ScoreFragment extends Fragment {
    private View view;
    private Activity activity;
    private ArrayList<String> high_score;

    public ScoreFragment() {
        high_score = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();
        Typeface custom_font = Typeface.createFromAsset(activity.getAssets(), "fonts/Handlee-Regular.ttf");
        view = inflater.inflate(R.layout.fragment_high_score, container, false);
        TextView score_header = (TextView) view.findViewById(R.id.score_header);
        score_header.setTypeface(custom_font);
        ListView score_lv = (ListView) view.findViewById(R.id.score_lv);
        high_score = LandingPage.db.getItems();
        score_lv.setAdapter(new ScoreListAdapter(activity, high_score, custom_font));
        return view;
    }
}

