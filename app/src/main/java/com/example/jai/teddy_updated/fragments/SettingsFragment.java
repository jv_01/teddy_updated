package com.example.jai.teddy_updated.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.activities.GameScreen;
import com.example.jai.teddy_updated.activities.LandingPage;

/**
 * Created by jai on 3/11/15.
 */
public class SettingsFragment extends Fragment{
    private View view;
    private Switch sound_status_switch;
    private Switch music_status_switch;
    private TextView tv1, tv2, tv3;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    public SettingsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings_screen,container, false);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Handlee-Regular.ttf");

        sound_status_switch = (Switch) view.findViewById(R.id.switch1);
        music_status_switch = (Switch) view.findViewById(R.id.switch2);

        sound_status_switch.setSelected(LandingPage.play_status);
        music_status_switch.setSelected(LandingPage.music_status);

        sharedPref = getActivity().getSharedPreferences(LandingPage.PREFS_NAME, getActivity().MODE_PRIVATE);
        editor = sharedPref.edit();

        tv1 = (TextView) view.findViewById(R.id.textView10);
        tv2 = (TextView) view.findViewById(R.id.textView9);
        tv3 = (TextView) view.findViewById(R.id.textView5);
        tv1.setTypeface(custom_font);
        tv2.setTypeface(custom_font);
        tv3.setTypeface(custom_font);

        sound_status_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked())
                    LandingPage.play_status = true;
                else
                    LandingPage.play_status = false;
                LandingPage.start_music();
                editor.putBoolean("play_status", buttonView.isChecked());
                editor.commit();
            }
        });
        music_status_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked())
                    LandingPage.music_status = true;
                else
                    LandingPage.music_status = false;
                editor.putBoolean("music_status", buttonView.isChecked());
                editor.commit();
            }
        });

        return view;
    }
}

