package com.example.jai.teddy_updated.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.activities.GameScreen;
import com.example.jai.teddy_updated.activities.LandingPage;

/**
 * Created by jai on 3/11/15.
 */
public class StartScreenFragment extends Fragment {

    private View view;
    private Activity activity;
    private Button start_btn;

    public StartScreenFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Handlee-Regular.ttf");
        view = inflater.inflate(R.layout.fragment_start_screen, container, false);
        start_btn = (Button) view.findViewById(R.id.start_button);
        start_btn.setTypeface(custom_font);
        start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, GameScreen.class));
                if(LandingPage.mediaPlayer.isPlaying())
                    LandingPage.playing_position = LandingPage.mediaPlayer.getCurrentPosition();
            }
        });
        return view;
    }
}
