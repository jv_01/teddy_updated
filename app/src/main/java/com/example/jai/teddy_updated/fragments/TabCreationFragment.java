package com.example.jai.teddy_updated.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.jai.teddy_updated.R;
import com.example.jai.teddy_updated.adapters.SectionsAdapter;

/**
 * Created by jai on 3/11/15.
 */
public class TabCreationFragment extends Fragment {

    private SectionsAdapter sectionsAdapter;
    public static ViewPager mViewPager;
    private ImageView _btn1, _btn2, _btn3;

    public TabCreationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_each_tab, container, false);
        sectionsAdapter = new SectionsAdapter(getActivity(), this.getChildFragmentManager());

        _btn1 = (ImageView) v.findViewById(R.id.btn1);
        _btn2 = (ImageView) v.findViewById(R.id.btn2);
        _btn3 = (ImageView) v.findViewById(R.id.btn3);
        _btn2.setImageResource(R.drawable.fill_circle);

        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(sectionsAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setCurrentItem(1);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        _btn1.setImageResource(R.drawable.fill_circle);
                        _btn2.setImageResource(R.drawable.holo_circle);
                        _btn3.setImageResource(R.drawable.holo_circle);
                        break;
                    case 1:
                        _btn1.setImageResource(R.drawable.holo_circle);
                        _btn2.setImageResource(R.drawable.fill_circle);
                        _btn3.setImageResource(R.drawable.holo_circle);
                        break;
                    case 2:
                        _btn1.setImageResource(R.drawable.holo_circle);
                        _btn2.setImageResource(R.drawable.holo_circle);
                        _btn3.setImageResource(R.drawable.fill_circle);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;

    }
}

