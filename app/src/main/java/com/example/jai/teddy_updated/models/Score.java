package com.example.jai.teddy_updated.models;

import java.io.Serializable;

/**
 * Created by jai on 21/10/15.
 */
public class Score implements Serializable {
    int score = 0;

    public int getScore() {return score;}
    public void setScore(int score) {this.score = score;}
}
